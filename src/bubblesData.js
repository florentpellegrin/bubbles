const BubblesData =
  [{
    label: 'Microsoft',
    amount: 650,
    children: [
        {label: 'Mobile', amount: 120},
        {label: 'C#', amount: 110},
        {
            label: 'Microsoft technologies', amount: 133,
            children: [
                {label: 'C#', amount: 50},
                {label: '.net', amount: 30},
                {label: 'VSTO', amount: 20}
            ]
        },
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 238},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 348},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {label: 'OneNote', amount: 128},
        {
            label: 'Microsoft Plateform Engineer', amount: 590,
            children: [
                {label: 'Competence 01', amount: 20},
                {label: 'Competence 02', amount: 20},
                {label: 'Competence 03', amount: 80},
                {label: 'Competence 04', amount: 10},
                {label: 'Competence 05', amount: 30}
            ]
        }
    ]
},
{
    label: 'Other Microsoft',
    amount: 133,
    children: [
        {
            label: '.net, C#, Microsoft technologies (ASP...)', amount: 133,
            children: [
                {label: 'NT', amount: 14},
                {label: 'OneNote', amount: 128},
                {label: 'Picture Manager', amount: 19}
            ]
        }
    ]
},
{
    label: 'SQL Server',
    amount: 650,
    children: [
        {
            label: 'C#', amount: 110,
            children: [
                {label: 'C# .NET', amount: 50},
                {label: 'C#.NET, VSTO', amount: 30},
                {label: 'C#1.1', amount: 20},
                {label: 'C#2.1', amount: 20},
                {label: 'C#3.1', amount: 20},
                {label: 'C# (General)', amount: 80}
            ]
        },
        {
            label: 'Microsoft Platform Engineer', amount: 590,
            children: [
                {label: 'AppFabric', amount: 10},
                {label: 'CE', amount: 12},
                {label: 'Entourage', amount: 132},
                {label: 'Forefront Endpoint Security', amount: 44},
                {label: 'Forefront Identity Management', amount: 189},
                {label: 'ForefrontApplicationProtection', amount: 27},
                {label: 'LDAP / ADS', amount: 33},
                {label: 'MapPoint', amount: 143},
                {label: 'Microsoft Server 2000', amount: 10},
                {label: 'Microsoft Server 2003', amount: 10},
                {label: 'Microsoft Server 2008', amount: 150},
                {label: 'Microsoft Server 2008 R2', amount: 117},
                {label: 'Microsoft Webservices UDDI', amount: 19},
                {label: 'Mobile', amount: 12}
            ]
        }
    ]
},
{
    label: 'Big Data',
    amount: 650,
    children: [
        {label: 'Categorie 01', amount: 120},
        {label: 'Categorie 02', amount: 110},
        {
            label: 'Categorie 03', amount: 233,
            children: [
                {label: 'Competence 01', amount: 50},
                {label: 'Competence 02', amount: 30},
                {label: 'Competence 03', amount: 20}
            ]
        },
        {label: 'Categorie 04', amount: 128},
        {
            label: 'Categorie 05', amount: 290,
            children: [
                {label: 'Competence 01', amount: 20},
                {label: 'Competence 02', amount: 20},
                {label: 'Competence 03', amount: 80},
                {label: 'Competence 04', amount: 10},
                {label: 'Competence 05', amount: 30}
            ]
        }
    ]
}];

export default BubblesData;
