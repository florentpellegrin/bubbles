import './styles/main.scss'

import BubblesRender from './d3Bubbles'

document.body.innerHTML = BubblesRender;

require('./footer')

if (module.hot) {
  module.hot.accept()
}
