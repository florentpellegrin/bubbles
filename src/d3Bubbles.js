/*global d3:true */
import * as d3 from "d3";
import BubblesData from './bubblesData'


  const CreateNavItems = () => {
    const BubblesNavItems = BubblesData.map((bubbleItem) => (
      `
        <div class="bubble-item primary animated ${bubbleItem.label}">
          <div class="name">${bubbleItem.label}</div>
          <div class="amount">${bubbleItem.amount}</div>
        </div >
      `)
    ).join('')

    const bubbleNavWrapper = `<div class="bubbleNav-wrapper">${BubblesNavItems}</div>`

    return bubbleNavWrapper;
  };



  const CreateBubbles = (BubblesData) => {
    const d3body = d3.select(document.body);

    d3body.append('div')
          .attr('class', 'bubble-wrapper')
    // const svgWrapper = d3.select('.bubble-wrapper')
    //                 .append('svg')
    //                 .attr('width', '100%')
    //                 .attr('height', '100%')
    //                 .attr('class', 'svg');
    //
    // console.log('d3', svgWrapper)
    //
    // return svgWrapper

    console.log('d3body', d3body)
  }

const BubblesRender = `
  ${CreateNavItems()}
  ${CreateBubbles(BubblesData)}
  `

  export default BubblesRender

// (function ($) {
//     $.fn.skillBubble = function (arrayBubbles, options) {
//         $(this).addClass('row');
//         $(this).append('<div class="bubble-wrapper col l8"></div>');
//         $(this).append('<div class="bubble-name-wrapper"></div>');
//         var bubbleWrapper = $('.bubble-wrapper')[0];
//
//         this.each(function () {
//
//             var width = bubbleWrapper.offsetWidth,
//                 height = bubbleWrapper.offsetWidth,
//                 minOfWH = Math.min(width, height) / 2,
//                 radius = (minOfWH > 200) ? radius = 200 / 2 : radius = minOfWH / 2;
//
//             //Options
//             var settings = $.extend({
//                 // The defaults.
//                 primaryColor: "#19A2D6",
//                 secondaryColor: "rgb(17, 97, 127)",
//                 tertiaryColor: "#f3792e",
//                 primaryOuterRadius: 2,
//                 secondaryOuterRadius: 1.5,
//                 bubbleSelector: true,
//                 dataTable: true,
//                 initFirst: true,
//                 secondaryLimitation: 20,
//                 tertiaryLimitation: 15
//             }, options);
//
//             //
//             var primaryOuterCircle = radius * settings.primaryOuterRadius,
//                 secondaryOuterCircle = radius * settings.secondaryOuterRadius,
//                 colors = {
//                     primary: settings.primaryColor,
//                     secondary: settings.secondaryColor,
//                     tertiary: settings.tertiaryColor
//                 };
//
//             //TODO move in appropriate
//             var lengthSecondaryCircleArray;
//             var currentIndex;
//             var firstTime;
//             var direction = false;
//             var currentPrimary;
//             var currentOtherArray;
//
//             var tooltip = d3.select("body")
//                 .append("div")
//                 .attr('class', 'skillTooltip')
//                 .style("position", "absolute")
//                 .style("z-index", "100")
//                 .style("visibility", "hidden");
//
//
//             var gSvg = d3.select('.bubble-wrapper')
//                 .append('svg')
//                 .attr('width', width)
//                 .attr('height', height)
//                 .attr('class', 'svg');
//
//             gSvg.append('clipPath')
//                 .attr('id', 'circleMask')
//                 .append('circle')
//                 .attr('r', width / 2)
//                 .attr('cx', width / 2)
//                 .attr('cy', height / 2);
//
//             var gBubbles = gSvg
//                 .append('g')
//                 .attr('class', 'mask')
//                 .attr('clip-path', 'url(#circleMask)')
//                 .append('g')
//                 .attr('class', 'gBubbles')
//                 .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
//
//             var gOther = gSvg
//                 .append('g')
//                 .attr('class', 'gOther');
//
//             var draw = function (bubbleData) {
//                 gBubbles.append("circle")
//                     .attr("class", "outerCircle")
//                     .attr("r", 0);
//
//                 var primaryElem = gBubbles.append("g")
//                     .attr("class", "primaryElem")
//                     .selectAll("circle")
//                     .data(bubbleData);
//
//                 lengthSecondaryCircleArray = bubbleData[0].children.length;
//                 if (settings.secondaryLimitation) {
//                     var dataSetSecondary;
//                     if (lengthSecondaryCircleArray >= settings.secondaryLimitation) {
//                         var sortedArray = _.sortBy(bubbleData[0].children, function (child) {
//                             return child.amount
//                         }).reverse();
//                         dataSetSecondary = _.first(sortedArray, settings.secondaryLimitation);
//                         lengthSecondaryCircleArray = settings.secondaryLimitation;
//                     } else {
//                         dataSetSecondary = _.sortBy(bubbleData[0].children, function (child) {
//                             return child.amount
//                         }).reverse();
//                     }
//                     defineOtherArray(bubbleData[0].children, dataSetSecondary);
//                     addOther();
//                 } else {
//                     dataSetSecondary = bubbleData[0].children;
//                 }
//
//                 var secondaryElem = gBubbles.append("g")
//                     .attr("class", "secondaryElem")
//                     .selectAll('circle')
//                     .data(dataSetSecondary);
//
//
//                 var primaryElemEnter = primaryElem.enter()
//                     .append("g")
//                     .attr('class', 'gPrimary');
//
//                 primaryElemEnter
//                     .append("circle")
//                     .attr("r", radius)
//                     .attr("class", "primaryCircle")
//                     .style("fill", colors.primary)
//                     .style('opacity', 0);
//
//                 primaryElemEnter
//                     .append("text")
//                     .attr('height', 'auto')
//                     .attr('text-anchor', 'middle')
//                     .attr('fill', 'white')
//                     .append('tspan')
//                     .attr('dy', 0)
//                     .attr("x", 0)
//                     .text(function (d) {
//                         return d.amount;
//                     })
//                     .select(function () {
//                         return this.parentNode;
//                     })
//                     .append('tspan')
//                     .attr('dy', 20)
//                     .attr("x", 0)
//                     .text(function (d) {
//                         return d.label;
//                     });
//                 var secondaryElemEnter = secondaryElem.enter()
//                     .append("g")
//                     .style('opacity', 0)
//                     .attr('class', 'gSecondary');
//                 secondaryElemEnter.append("circle")
//                     .attr('class', 'secondaryCircle')
//                     .attr("r", function (d) {
//
//                         return defineRadius(d, secondaryElem.data());
//                     })
//                     .attr('data-label', function (d) {
//                         return d.label
//                     })
//                     .attr("fill", colors.secondary)
//                     .attr('stroke', 'white');
//
//                 secondaryElemEnter.append("text")
//                     .attr('text-anchor', 'middle')
//                     .attr('fill', 'white')
//                     .attr("pointer-events", "none")
//                     .attr('dy', 5)
//                     /*.append('tspan')
//
//                      .attr("x", 0)*/
//                     .text(function (d) {
//                         return d.amount;
//                     });
//
//                 primaryElem.each(function (d) {
//                     var c = d3.select(this);
//                     if (d.children) {
//                         innerCircle(c);
//                     }
//                 });
//                 secondaryElem.each(function (d, i) {
//                     var c = d3.select(this);
//                     if (d.children) {
//                         innerCircle(c);
//                         c.style("cursor", "pointer");
//                     }
//                 });
//
//                 d3.select(".primaryCircle")
//                     .transition()
//                     .duration(750)
//                     .style("opacity", 1);
//
//                 d3.select(".outerCircle")
//                     .transition()
//                     .duration(750)
//                     .attr("r", primaryOuterCircle);
//
//                 d3.selectAll(".gSecondary")
//                     .transition()
//                     .duration(750)
//                     .style('opacity', 1)
//                     .attr("transform", function (d, i) {
//                         return "translate(" + defineCx(d, i, lengthSecondaryCircleArray, primaryOuterCircle) + ", " + defineCy(d, i, lengthSecondaryCircleArray, primaryOuterCircle) + ") ";
//                     });
//
//                 $(".bubble-name-wrapper").html(bubbleData[0].label);
//
//             };
//
//
//
//
//             /************************************************/
//             //Functions
//             /************************************************/
//             function addOther() {
//                 creategOther();
//                 gOtherTransition();
//
//                 gOther
//                     .on('click', clickOther);
//
//             }
//
//             function creategOther() {
//                 var otherArray = [];
//                 currentOtherArray.forEach(function (d, i) {
//                     otherArray.push(d.amount);
//                 });
//                 var totalOther = 0;
//                 otherArray.forEach(function (d, i) {
//                     totalOther = totalOther + d;
//                 });
//                 if (currentOtherArray.length > 0) {
//                     //OTHER
//                     gOther
//                         .style('opacity', 0)
//                         .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
//                     var otherElem = gOther
//                         .append("g")
//                         .attr('class', 'gSecondaryOther');
//                     otherElem
//                         .append('circle')
//                         .attr('class', 'secondaryCircle other')
//                         .attr("r", 50)
//                         .attr('data-label', 'Others')
//                         .attr("fill", colors.secondary)
//                         .attr('stroke-width', 5);
//
//                     otherElem
//                         .append("text")
//                         .attr('text-anchor', 'middle')
//                         .attr('fill', 'white')
//                         .attr("pointer-events", "none")
//                         .attr('dy', 5)
//                         .text(totalOther);
//                     //END OTHER
//
//                     otherElem
//                         .on("mouseover", mouseOverSecondary)
//                         .on('mousemove', mouseMoveSecondary)
//                         .on("mouseout", mouseOutSecondary);
//                 }
//             }
//
//             function defineOtherArray(firstArr, secondArr) {
//                 currentOtherArray = _.difference(firstArr, secondArr);
//             }
//
//             function clickOther() {
//                 hidegBubbles();
//                 addPreviousButton();
//
//                 function hidegBubbles() {
//                     gBubbles
//                         .transition()
//                         .duration(750)
//                         .style('opacity', 0)
//                         .each('end', visibilityHidden)
//                 }
//
//                 function visibilityHidden() {
//                     gBubbles
//                         .style('visibility', 'hidden');
//                 }
//
//                 function addPreviousButton() {
//                     var posPrevY = -(height / 2 - 40);
//                     gOther
//                         .append('g')
//                         .attr('width', 30)
//                         .attr('height', 30)
//                         .attr('class', 'previousButton')
//                         .style('opacity', 0)
//                         .attr("transform", "translate(0, " + posPrevY + ")")
//                         .append('circle')
//                         .style('fill', colors.primary)
//                         .attr('r', 20)
//                         .select(function () {
//                             return this.parentNode;
//                         })
//                         .append('text')
//                         .attr('text-anchor', 'middle')
//                         .attr('fill', 'white')
//                         .attr("pointer-events", "none")
//                         .attr('dy', 5)
//                         .text('back');
//                 }
//
//                 function showPreviousButton() {
//                     d3.select('.previousButton')
//                         .transition()
//                         .duration(750)
//                         .style('opacity', 1);
//                 }
//
//                 function addOuter() {
//                     gOther
//                         .append("circle")
//                         .attr("class", "otherOuterCircle")
//                         .attr("pointer-events", "none")
//                         .attr("r", 0);
//
//                     d3.select('.otherOuterCircle')
//                         .transition()
//                         .duration(750)
//                         .attr("r", primaryOuterCircle)
//                         .each('end', showPreviousButton);
//
//                     addSecondaries();
//                 }
//
//                 function addSecondaries() {
//                     var otherDataSet = _.sortBy(currentOtherArray, function (child) {
//                         return child.amount
//                     }).reverse();
//
//                     var otherSecondaryElem = gOther.append("g")
//                         .attr("class", "otherSecondaryElem")
//                         .selectAll('circle')
//                         .data(otherDataSet);
//
//                     var otherSecondaryElemEnter = otherSecondaryElem.enter()
//                         .append("g")
//                         .style('opacity', 0)
//                         .attr('class', 'gOtherSecondary');
//                     otherSecondaryElemEnter.append("circle")
//                         .attr('class', 'secondaryCircle')
//                         .attr("r", function (d) {
//
//                             return defineRadiusOther(d, otherSecondaryElem.data());
//                         })
//                         .attr('data-label', function (d) {
//                             return d.label
//                         })
//                         .attr("fill", colors.secondary)
//                         .attr('stroke', 'white');
//
//                     otherSecondaryElemEnter.append("text")
//                         .attr('text-anchor', 'middle')
//                         .attr('fill', 'white')
//                         .attr("pointer-events", "none")
//                         .attr('dy', 5)
//                         .text(function (d) {
//                             return d.amount;
//                         });
//
//                     otherSecondaryElem.each(function (d, i) {
//                         var c = d3.select(this);
//                         if (d.children) {
//                             innerCircle(c);
//                             c.style("cursor", "pointer");
//                         }
//                     });
//
//                     d3.selectAll(".gOtherSecondary")
//                         .transition()
//                         .duration(750)
//                         .style('opacity', 1)
//                         .attr("transform", function (d, i) {
//                             return "translate(" + defineCx(d, i, currentOtherArray.length, primaryOuterCircle) + ", " + defineCy(d, i, currentOtherArray.length, primaryOuterCircle) + ") ";
//                         });
//
//
//
//                     d3.selectAll(".gOtherSecondary")
//                         .on("mouseover", mouseOverSecondary)
//                         .on('mousemove', mouseMoveSecondary)
//                         .on("mouseout", mouseOutSecondary);
//                 }
//
//                 gOther
//                     .transition()
//                     .duration(750)
//                     .attr("transform", function (d, i) {
//                         var posOtherX = width / 2;
//                         var posOtherY = height / 2;
//                         return "translate(" + posOtherX + " , " + posOtherY + ")";
//                     })
//                     .each('end', addOuter);
//
//                 if (d3.select('.previousButton')) {
//                     d3.select('.previousButton')
//                         .on('click', clickPreviousOther);
//                 }
//
//                 d3.select('.gSecondaryOther circle')
//                     .transition()
//                     .duration(750)
//                     .attr('stroke-width', 0);
//
//                 gOther.on('click', null);
//             }
//
//             function clickPreviousOther() {
//                 d3.event.stopPropagation();
//                 function showgBubbles() {
//                     gBubbles
//                         .transition()
//                         .duration(750)
//                         .style('opacity', 1)
//                         .style('visibility', 'visible');
//                     gOther
//                         .on('click', clickOther);
//                 }
//
//                 d3.select('.otherOuterCircle')
//                     .call(setupRemove)
//                     .transition()
//                     .duration(750)
//                     .attr("r", 0)
//                     .each('end', onRemove)
//                     .remove();
//
//                 function setupRemove(sel) {
//                     counter = sel.size();
//                 }
//
//                 function onRemove() {
//                     counter--;
//                     if (counter == 0) {
//                         //console.log("all done");
//                         showgBubbles();
//                         gOtherTransition();
//                     }
//                 }
//
//                 d3.select('.previousButton')
//                     .call(setupRemove)
//                     .transition()
//                     .duration(750)
//                     .style("opacity", 0)
//                     .each('end', onRemove)
//                     .remove();
//
//                 d3.selectAll(".gOtherSecondary")
//                     .transition()
//                     .duration(750)
//                     .attr("transform", "translate(0,0)");
//
//                 d3.selectAll(".otherSecondaryElem")
//                     .call(setupRemove)
//                     .transition()
//                     .duration(750)
//                     .style('opacity', 0)
//                     .each('end', onRemove)
//                     .remove();
//             }
//
//             function gOtherTransition() {
//                 d3.select('.gSecondaryOther circle')
//                     .transition()
//                     .duration(750)
//                     .attr('stroke-width', 5);
//
//                gOther
//                     .transition()
//                     .duration(750)
//                     .style('opacity', 1)
//                     .attr("transform", function (d, i) {
//                         var posOtherX = width / 2 + width / 2 * Math.cos(45 / 180 * Math.PI);
//                         var posOtherY = height / 2 + height / 2 * Math.sin(45 / 180 * Math.PI);
//                         return "translate(" + posOtherX + " , " + posOtherY + ")";
//                     });
//             }
//
//             function removegOther() {
//                 gOther.html('');
//             }
//
//             function defineCx(d, i, arrayLength, outerRadius) {
//                 var deg = (i * 360 ) / arrayLength;
//                 return outerRadius * Math.cos(deg / 180 * Math.PI);
//             }
//
//             function defineCy(d, i, arrayLength, outerRadius) {
//                 var deg = (i * 360 ) / arrayLength;
//                 return outerRadius * Math.sin(deg / 180 * Math.PI);
//             }
//
//             function defineRadius(elem, groupElems) {
//                 var groupArray = [];
//                 groupElems.forEach(function (d, i) {
//                     groupArray.push(d.amount);
//                 });
//                 max = Math.max.apply(null, groupArray);
//                 var elRadius = (elem.amount * 100 / max) * 50 / 100;
//                 if (elRadius < 15) elRadius = 15;
//
//                 return elRadius;
//             }
//
//             function defineRadiusOther(elem, groupElems) {
//                 var groupArray = [];
//                 groupElems.forEach(function (d, i) {
//                     groupArray.push(d.amount);
//                 });
//                 max = Math.max.apply(null, groupArray);
//                 var elRadius = (elem.amount * 100 / max) * 30 / 100;
//                 if (elRadius < 15) elRadius = 15;
//
//                 return elRadius;
//             }
//
//             function innerCircle(gElem) {
//                 var circleRadius = gElem.select('circle').attr('r');
//                 gElem.append("circle")
//                     .attr("class", "innerCircle")
//                     .attr("r", function () {
//                         return circleRadius - 5 + 'px';
//                     });
//             }
//
//             function clickPrimaryCircle() {
//                 if (gBubbles.classed('secondaryZoomed')) {
//                     reset();
//                     addOther();
//                     changeDataTable(currentPrimary);
//                 }
//             }
//
//             function defineDirection(elem) {
//                 if (elem.classed('previous')) {
//                     direction = 'up';
//                 } else {
//                     direction = 'down';
//                 }
//             }
//
//             function clickSecondaryCircle(d, i) {
//                 var currentSecondary = d3.select(this);
//                 currentIndex = i;
//
//                 removegOther();
//                 defineDirection(currentSecondary);
//
//                 d3.selectAll('.gSecondary')
//                     .attr('class', 'gSecondary');
//
//                 var previousSibling = d3.select(this.previousSibling),
//                     previousSiblingRestart = d3.select('.gSecondary:nth-child(' + lengthSecondaryCircleArray + ')'),
//                     nextSibling = d3.select(this.nextSibling),
//                     nextSiblingRestart = d3.select('.gSecondary:nth-child(' + 1 + ')');
//
//                 (previousSibling[0][0] === null) ? previousSiblingRestart.classed('previous', true) : previousSibling.classed('previous', true);
//                 currentSecondary.classed('current', true);
//                 (nextSibling[0][0] === null) ? nextSiblingRestart.classed('next', true) : nextSibling.classed('next', true);
//
//
//
//                 if (!gBubbles.classed('secondaryZoomed')) {
//                     firstTime = true;
//                     //transition to left
//                     gBubbles.transition()
//                         .duration(750)
//                         .attr('transform', 'translate(' + (width - (width + currentSecondary.attr('r'))) + ',' + height / 2 + ')')
//                         .attr('class', 'secondaryZoomed');
//
//                     //transition outer circle
//                     d3.select('.outerCircle')
//                         .transition()
//                         .duration(750)
//                         .attr("pointer-events", "none")
//                         .attr('r', primaryOuterCircle = primaryOuterCircle * 2);
//
//                     //add tertiary circles
//                     if (currentSecondary.data()[0].children) {
//                         addTertiary(currentSecondary)
//                     }
//
//                     //transition rotation secondaries
//                     d3.selectAll('.gSecondary')
//                         .transition()
//                         .duration(750)
//                         .attrTween('transform', angleTween);
//
//                     d3.selectAll('.gSecondary:not(.previous):not(.current):not(.next)')
//                         .transition()
//                         .duration(750)
//                         .style('opacity', 0);
//
//
//                 } else {
//                     firstTime = false;
//
//                     d3.selectAll('.secondaryOuterCircle')
//                         .transition()
//                         .duration(750)
//                         .attr("r", 0)
//                         .style('opacity', 0)
//                         .remove();
//
//                     d3.selectAll('.gTertiary:not(.current)')
//                         .transition()
//                         .duration(750)
//                         .attr("r", 0)
//                         .attr('transform', 'translate(0,0)')
//                         .style('opacity', 0)
//                         .remove();
//
//                     if (currentSecondary.data()[0].children) {
//                         addTertiary(currentSecondary)
//                     }
//
//                     d3.selectAll('.gSecondary')
//                         .transition()
//                         .duration(750)
//                         .style('opacity', 1)
//                         .attrTween('transform', angleTween);
//
//
//                     d3.selectAll('.gSecondary:not(.previous):not(.current):not(.next)')
//                         .transition()
//                         .duration(750)
//                         .style('opacity', 0)
//                 }
//
//                 changeDataTable(currentPrimary, currentSecondary.data()[0]);
//             }
//
//             function addTertiary(elem) {
//                 var elemChildren = elem.data()[0].children;
//                 var lengthTertiaryCircleArray = elemChildren.length;
//
//                 if (settings.tertiaryLimitation) {
//                     var dataSetTertiary;
//                     if (lengthTertiaryCircleArray >= settings.tertiaryLimitation) {
//                         var sortedArray = _.sortBy(elemChildren, function (child) {
//                             return child.amount
//                         }).reverse();
//                         dataSetTertiary = _.first(sortedArray, settings.tertiaryLimitation - 1);
//                         lengthTertiaryCircleArray = settings.tertiaryLimitation;
//                     } else {
//                         dataSetTertiary = _.sortBy(elemChildren, function (child) {
//                             return child.amount
//                         }).reverse();
//                     }
//                 } else {
//                     dataSetTertiary = elemChildren;
//                 }
//
//
//                 elem
//                     .append("circle")
//                     .attr("class", "secondaryOuterCircle")
//                     .attr("r", elem.select('circle').attr('r'))
//                     .style('opacity', 0)
//                     .transition()
//                     .duration(750)
//                     .attr("pointer-events", "none")
//                     .attr("r", secondaryOuterCircle)
//                     .style('opacity', 1);
//
//                 dataSetTertiary.forEach(function (d, i) {
//                     elem
//                         .append('g')
//                         .attr('class', 'gTertiary')
//                         .style('opacity', 0)
//                         .append('circle')
//                         .attr('class', 'tertiaryCircle')
//                         .attr('r', defineRadius(d, elemChildren))
//                         .attr('fill', colors.tertiary)
//                         .attr('data-label', function () {
//                             return d.label
//                         })
//                         .select(function () {
//                             return this.parentNode;
//                         })
//                         .append('text')
//                         .attr('text-anchor', 'middle')
//                         .attr('fill', 'white')
//                         .attr('dy', 5)
//                         .attr("x", 0)
//                         .attr("pointer-events", "none")
//                         .text(d.amount);
//                 });
//
//                 d3.select('.gSecondary.current').selectAll(".gTertiary")
//                     .transition()
//                     .duration(750)
//                     .style('opacity', 1)
//                     .attr("transform", function (d, i) {
//                         return "translate(" + defineCx(d, i, lengthTertiaryCircleArray, secondaryOuterCircle) + ", " + defineCy(d, i, lengthTertiaryCircleArray, secondaryOuterCircle) + ") ";
//                     });
//                 d3.selectAll(".gTertiary circle")
//                     .on("mouseover", mouseOverTertiary)
//                     .on('mousemove', mouseMoveTertiary)
//                     .on("mouseout", mouseOutTertiary);
//             }
//
//             function angleTween(d, i) {
//                 var t_angle;//end position
//                 var previousIndex = ((currentIndex - 1) < 0) ? lengthSecondaryCircleArray - 1 : (currentIndex - 1);
//                 var nextIndex = ((currentIndex + 1) > lengthSecondaryCircleArray - 1) ? 0 : (currentIndex + 1);
//                 var initialPositionInRad;
//                 var prevRad;
//                 var curRad;
//                 var nextRad;
//
//
//                 return function (t) {
//                     var rotation_radius;
//
//                     if (((currentIndex * 360 / lengthSecondaryCircleArray) / 180 * Math.PI) >= Math.PI) {
//                         prevRad = 1.75 * Math.PI;
//                         curRad = 2 * Math.PI;
//                         if (currentIndex === lengthSecondaryCircleArray - 1) {
//                             nextRad = 0.25 * Math.PI;
//                         } else {
//                             nextRad = 2.25 * Math.PI;
//                         }
//                     } else {
//                         if (currentIndex === 0) {
//                             prevRad = 1.75 * Math.PI;
//                         } else {
//                             prevRad = -0.25 * Math.PI;
//                         }
//                         curRad = 0;
//                         nextRad = 0.25 * Math.PI;
//                     }
//
//                     if (firstTime) {
//                         rotation_radius = radius * settings.primaryOuterRadius + (primaryOuterCircle - radius * settings.primaryOuterRadius) * t;
//                         initialPositionInRad = (i * 360 / lengthSecondaryCircleArray) / 180 * Math.PI;
//                         if (i === currentIndex) {
//                             t_angle = initialPositionInRad + (curRad - initialPositionInRad) * t;
//                         } else if (i === previousIndex) {
//                             t_angle = initialPositionInRad + (prevRad - initialPositionInRad) * t;
//                         } else if (i === nextIndex) {
//                             t_angle = initialPositionInRad + (nextRad - initialPositionInRad) * t;
//                         } else {
//                             t_angle = initialPositionInRad;
//                         }
//                     } else {
//                         rotation_radius = primaryOuterCircle;
//                         initialPositionInRad = (i * 360 / lengthSecondaryCircleArray) / 180 * Math.PI;
//
//                         if (direction === 'up') {
//                             if (i === currentIndex) {
//                                 t_angle = Math.PI * -0.25 + (0 - Math.PI * -0.25) * t;
//                             } else if (i === previousIndex) {
//                                 t_angle = Math.PI * -0.5 + (Math.PI * -0.25 - (Math.PI * -0.5)) * t;
//                             } else if (i === nextIndex) {
//                                 t_angle = Math.PI * 0.25 * t;
//                             }
//                         } else {
//                             if (i === currentIndex) {
//                                 t_angle = Math.PI * 0.25 + (0 - Math.PI * 0.25) * t;
//                             } else if (i === previousIndex) {
//                                 t_angle = Math.PI * -0.25 * t;
//                             } else if (i === nextIndex) {
//                                 t_angle = Math.PI * 0.5 + (Math.PI * 0.25 - (Math.PI * 0.5)) * t;
//                             }
//                         }
//                     }
//
//                     var t_x = rotation_radius * Math.cos(t_angle);
//                     var t_y = rotation_radius * Math.sin(t_angle);
//                     return "translate(" + t_x + "," + t_y + ")";
//                 };
//
//             }
//
//             function angleTN(d, i, a) {
//                 var t_angle;
//                 var previousIndex = ((currentIndex - 1) < 0) ? lengthSecondaryCircleArray - 1 : (currentIndex - 1);
//                 var nextIndex = ((currentIndex + 1) > lengthSecondaryCircleArray - 1) ? 0 : (currentIndex + 1);
//                 var finalPositionInRad = i * 360 / lengthSecondaryCircleArray / 180 * Math.PI;
//                 var prevRad;
//                 var curRad;
//                 var nextRad;
//
//                 if (((currentIndex * 360 / lengthSecondaryCircleArray) / 180 * Math.PI) >= Math.PI) {
//                     prevRad = 1.75 * Math.PI;
//                     curRad = 2 * Math.PI;
//                     (currentIndex === lengthSecondaryCircleArray - 1) ? nextRad = 0.25 * Math.PI : nextRad = 2.25 * Math.PI;
//                 } else {
//                     (currentIndex === 0) ? prevRad = 1.75 * Math.PI : prevRad = -0.25 * Math.PI;
//                     curRad = 0;
//                     nextRad = 0.25 * Math.PI;
//                 }
//                 return function (t) {
//                     if (i === currentIndex) {
//                         t_angle = curRad + (finalPositionInRad - curRad) * t;
//                     } else if (i === previousIndex) {
//                         t_angle = prevRad + (finalPositionInRad - prevRad) * t;
//                     } else if (i === nextIndex) {
//                         t_angle = nextRad + (finalPositionInRad - nextRad) * t;
//                     } else {
//                         t_angle = Math.PI + (finalPositionInRad - Math.PI) * t;
//                     }
//
//                     var rotation_radius = (radius * settings.primaryOuterRadius) * (2 - t);//t = 2 - t; to reverse animation
//                     if (rotation_radius > primaryOuterCircle) rotation_radius = primaryOuterCircle;
//                     var t_x = rotation_radius * Math.cos(t_angle) * (2 - t);
//                     var t_y = rotation_radius * Math.sin(t_angle) * (2 - t);
//                     return "translate(" + t_x + "," + t_y + ")";
//                 }
//             }
//
//             function mouseMoveSecondary() {
//                 var c = d3.select(this);
//                 if (c.data()[0]) {
//                     var elementLabel = c.data()[0].label;
//                 } else {
//                     elementLabel = 'Others'
//                 }
//
//                 tooltip
//                     .style("top", (d3.event.pageY - 25) + "px")
//                     .style("left", (d3.event.pageX + 15) + "px")
//                     .classed('tertiary', false)
//                     .classed('secondary', true)
//                     .text(elementLabel)
//             }
//
//             function mouseOverSecondary() {
//                 tooltip.style("visibility", "visible");
//             }
//
//             function mouseOutSecondary() {
//                 tooltip.style("visibility", "hidden");
//             }
//
//             function mouseMoveTertiary(d) {
//                 d3.event.stopPropagation();
//                 var c = d3.select(this);
//                 var elementLabel = c.attr('data-label');
//                 tooltip
//                     .style("top", (d3.event.pageY - 25) + "px")
//                     .style("left", (d3.event.pageX + 15) + "px")
//                     .classed('secondary', false)
//                     .classed('tertiary', true)
//                     .text(elementLabel)
//             }
//
//             function mouseOverTertiary() {
//                 d3.event.stopPropagation();
//                 tooltip.style("visibility", "visible");
//             }
//
//             function mouseOutTertiary() {
//                 d3.event.stopPropagation();
//                 tooltip.style("visibility", "hidden");
//             }
//
//             function reset() {
//                 firstTime = true;
//
//                 gBubbles
//                     .transition()
//                     .duration(750)
//                     .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')
//                     .attr('class', '');
//
//                 d3.select('.outerCircle')
//                     .transition()
//                     .duration(750)
//                     .attr('r', primaryOuterCircle = radius * settings.primaryOuterRadius);
//
//                 d3.selectAll('.secondaryOuterCircle')
//                     .transition()
//                     .duration(750)
//                     .attr("r", 0)
//                     .style('opacity', 0)
//                     .remove();
//
//                 d3.selectAll('.gTertiary')
//                     .transition()
//                     .duration(250)
//                     .attr("r", 0)
//                     .attr('transform', 'translate(0,0)')
//                     .style('opacity', 0)
//                     .remove();
//
//                 d3.selectAll('.gSecondary')
//                     .transition()
//                     .duration(750)
//                     .style('opacity', 1)
//                     .attr('class', 'gSecondary')
//                     .attrTween('transform', angleTN)
//             }
//
//             /************************************************/
//             //Bubble Selector
//             /************************************************/
//             if (settings.bubbleSelector) {
//                 $(this).prepend('<div class="bubble-selector"></div>');
//                 function addButtons(arrayEl) {
//                     arrayEl.forEach(function (val, i) {
//                         $('.bubble-selector').append("<button class='bubbleButton' title='" + val[0].label + "'>" + val[0].label + "</button>");
//                     });
//
//                     var bubbleButtons = $('.bubbleButton');
//                     for (var i = 0, len = bubbleButtons.length; i < len; i++) {
//                         (function (index) {
//
//                             bubbleButtons[i].onclick = function () {
//                                 gBubbles.html('');//reset svg
//                                 bubbleButtons.removeClass('active');
//                                 removegOther();
//                                 reset();
//                                 currentPrimary = arrayEl[index][0];
//                                 changeDataTable(currentPrimary);
//                                 draw(arrayEl[index]);
//                                 $(".bubble-name-wrapper").html(arrayEl[index][0].label);
//                                 $(this).addClass('active');
//                             }
//                         })(i);
//                     }
//                 }
//
//                 addButtons(bubblesArray);
//             } else {
//                 if ($('.bubble-selector')) {
//                     $('.bubble-selector').remove();
//                 }
//             }
//
//             /************************************************/
//             //Data Table
//             /************************************************/
//             if (settings.dataTable) {
//                 $(this).append('<div class="bubble-dataTable-wrapper col l3 offset-l1"></div>');
//                 var dataTable = $('.bubble-dataTable-wrapper');
//
//                 dataTable.append('<div class="primaries"></div>');
//                 dataTable.append('<div class="secondaries"></div>');
//                 dataTable.append('<div class="tertiaries"></div>');
//
//                 function resetDataTable() {
//                     $('.primaries').html('');
//                     $('.secondaries').html('');
//                     $('.tertiaries').html('');
//                 }
//
//                 function changeDataTable(currentPrimary, currentSecondary) {
//                     resetDataTable();
//                     if (!currentSecondary || currentSecondary === 'undefined') {
//                         if (!currentPrimary || currentPrimary === 'undefined') {
//                             var primaryArraySorted = _.sortBy(arrayBubbles, function (child) {
//                                 return child[0].amount
//                             }).reverse();
//                             var elementsPrimary = '';
//                             primaryArraySorted.forEach(function (entry) {
//                                 elementsPrimary += '<li class="primary ' + entry[0].label + ' row"><span class="label truncate col l9">' + entry[0].label + '</span><span class="amount col l3">' + entry[0].amount + '</span></li>'
//                             });
//
//                             $(".primaries").html('<ul>' + elementsPrimary + '</ul>').css('height', '90%');
//                         } else {
//                             var secondaryArraySorted = _.sortBy(currentPrimary.children, function (child) {
//                                 return child.amount
//                             }).reverse();
//                             var elementsSecondary = '';
//                             secondaryArraySorted.forEach(function (entry) {
//                                 elementsSecondary += '<li class="secondary ' + entry.label + ' row"><span class="label truncate col l9">' + entry.label + '</span><span class="amount col l3">' + entry.amount + '</span></li>'
//                             });
//                             $('.primaries').html('<div class="title primary row"><span class="label truncate col l9">' + currentPrimary.label + '</span><span class="amount col l3">' + currentPrimary.amount + '</span></div>').css('height', 'auto');
//                             $(".secondaries").html('<ul>' + elementsSecondary + '</ul>').css('height', '90%');
//                         }
//                     } else {
//                         var tertiaryArraySorted = _.sortBy(currentSecondary.children, function (child) {
//                             return child.amount
//                         }).reverse();
//                         var elementsTertiary = '';
//                         if (currentSecondary.children) {
//                             tertiaryArraySorted.forEach(function (entry) {
//                                 elementsTertiary += '<li class="tertiary ' + entry.label + ' row"><span class="label truncate col l9">' + entry.label + '</span><span class="amount col l3">' + entry.amount + '</span></li>'
//                             });
//                             $(".tertiaries").html('<ul>' + elementsTertiary + '</ul>').css('height', '90%');
//                         }
//                         $('.primaries').html('<div class="title primary row"><span class="label truncate col l9">' + currentPrimary.label + '</span><span class="amount col l3">' + currentPrimary.amount + '</span></div>').css('height', 'auto');
//                         $('.secondaries').html('<div class="title secondary row"><span class="label truncate col l9">' + currentSecondary.label + '</span><span class="amount col l3">' + currentSecondary.amount + '</span></div>').css('height', 'auto');
//
//                     }
//                 }
//
//                 changeDataTable();
//             } else {
//                 if (dataTable) {
//                     dataTable.remove();
//                 }
//             }
//             /************************************************/
//             //Initialisation with first data
//             /************************************************/
//             if (settings.initFirst) {
//                 draw(bubblesArray[0]);
//                 $('.bubbleButton:nth-child(1)').addClass('active');
//
//                 currentPrimary = bubblesArray[0][0];
//                 changeDataTable(currentPrimary);
//             }
//
//             /************************************************/
//             //Listeners
//             /************************************************/
//             d3.selectAll(".gPrimary")
//                 .style("cursor", "pointer")
//                 .on('click', clickPrimaryCircle);
//
//             d3.selectAll(".gSecondary")
//                 .on('click', clickSecondaryCircle);
//
//             d3.selectAll(".gSecondary")
//                 .on("mouseover", mouseOverSecondary)
//                 .on('mousemove', mouseMoveSecondary)
//                 .on("mouseout", mouseOutSecondary);
//
//             /************************************************/
//             //On resize
//             /************************************************/
//             /*$(window).resize(function(){
//              gBubbles.html('');//reset svg
//              reset();
//              draw(bubblesArray[0]);
//              })*/
//
//         });
//         return this;
//     };
// })(jQuery);
